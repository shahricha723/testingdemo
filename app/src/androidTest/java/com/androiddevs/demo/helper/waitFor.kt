package com.androiddevs.demo.helper

import android.view.View
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.ViewMatchers.isRoot

fun waitFor(delay: Long): ViewAction?
{
    return object : ViewAction
    {
        override fun getConstraints(): org.hamcrest.Matcher<View> = isRoot()
        override fun getDescription(): String = "wait for $delay milliseconds"
        override fun perform(uiController: UiController, v: View?)
        {
            uiController.loopMainThreadForAtLeast(delay)
        }
    }
}