package com.androiddevs.demo.di

import com.androiddevs.demo.data.model.Profile
import com.androiddevs.demo.repositories.DataRepository

class FakeDataRepoImpl : DataRepository
{

    override fun getProfile(): Profile =
        Profile(name = "Fake Name", age = 43)
}