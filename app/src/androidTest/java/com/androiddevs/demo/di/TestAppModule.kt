package com.androiddevs.demo.di

import android.content.Context
import androidx.room.Room
import com.androiddevs.demo.data.local.ShoppingItemDatabase
import com.androiddevs.demo.data.mock.MockInterceptor
import com.androiddevs.demo.other.Constants
import com.localebro.okhttpprofiler.OkHttpProfilerInterceptor
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
open class TestAppModule {

    @Provides
    @Named("test_db")
    fun provideInMemoryDb(@ApplicationContext context: Context) =
        Room.inMemoryDatabaseBuilder(context, ShoppingItemDatabase::class.java)
            .allowMainThreadQueries()
            .build()

  /*  @Singleton
    @Provides
    @Named("test_api")
    fun providePixabayApi(): PixabayAPI
    {

        return Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl(Constants.BASE_URL)
            .build()
            .create(PixabayAPI::class.java)

    }*/

    @Provides
    @Named("Normal")
    fun getRetrofit(@Named("NormalOkHttpClient") okHttpClient: OkHttpClient): Retrofit {
        /*var objectMapper=ObjectMapper()
        objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_ARRAY_AS_NULL_OBJECT)*/
        val builder = Retrofit.Builder()
        builder.baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
        builder.client(okHttpClient)
        return builder.build()
    }

    @Provides
    @Singleton
    @Named("OkHttpClient")
    fun provideOkHttpClient(
        @Named("MockInterceptor") authInterceptor: Interceptor
    ): OkHttpClient = OkHttpClient
        .Builder()
        .addInterceptor(OkHttpProfilerInterceptor())
        .addInterceptor(authInterceptor)
        .build()

    @Provides
    @Singleton
    @Named("MockInterceptor")
    fun provideMockInterceptor(): Interceptor = MockInterceptor()
}