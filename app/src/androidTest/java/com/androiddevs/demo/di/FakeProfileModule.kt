package com.androiddevs.demo.di

import com.androiddevs.demo.repositories.DataRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.testing.TestInstallIn


@TestInstallIn(
    components = [ViewModelComponent::class],
    replaces = [ProfileModule::class]
)
@Module
class FakeProfileModule {

    @Provides
    fun getProfileSource(): DataRepository = FakeDataRepoImpl()

}