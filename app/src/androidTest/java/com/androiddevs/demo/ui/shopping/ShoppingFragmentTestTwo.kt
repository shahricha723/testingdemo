package com.androiddevs.demo.ui.shopping

import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.MediumTest
import com.androiddevs.demo.R
import com.androiddevs.demo.adapters.ShoppingItemAdapter
import com.androiddevs.demo.data.local.ShoppingItem
import com.androiddevs.demo.getOrAwaitValue
import com.androiddevs.demo.helper.launchFragmentInHiltContainer
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.Timeout
import org.mockito.Mockito
import java.util.concurrent.TimeUnit

@MediumTest
@HiltAndroidTest
@ExperimentalCoroutinesApi
class ShoppingFragmentTestTwo
{

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    var globalTimeout: Timeout = Timeout(1, TimeUnit.MINUTES)

   /* @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
*/
    @Before
    fun setup()
    {
        hiltRule.inject()

    }

    @Test
    fun swipeShoppingItem_deleteItemInDb() {
        val shoppingItem = ShoppingItem("TEST", 1, 1f, "TEST", 1)
        var testViewModel: ShoppingViewModel? = null
        launchFragmentInHiltContainer<ShoppingFragment>(
        ) {
            testViewModel = viewModel
            CoroutineScope(Dispatchers.IO).launch {
                viewModel.insertShoppingItemIntoDb(shoppingItem)
            }
        }

        onView(withId(R.id.rvShoppingItems)).perform(
            RecyclerViewActions.actionOnItemAtPosition<ShoppingItemAdapter.ShoppingItemViewHolder>(
                0,
                ViewActions.swipeLeft()
            )
        )
        CoroutineScope(Dispatchers.IO).launch {
            assertThat(testViewModel?.shoppingItems?.getOrAwaitValue()).isEmpty()
        }
    }

    @Test
    fun clickAddShoppingItemButton_navigateToAddShoppingItemFragment() {
        val navController = Mockito.mock(NavController::class.java)

        launchFragmentInHiltContainer<ShoppingFragment>(
        ) {
            Navigation.setViewNavController(requireView(), navController)
        }

        onView(withId(R.id.fabAddShoppingItem)).perform(ViewActions.click())
//        onView(ViewMatchers.isRoot()).perform(waitFor(3000))

        Mockito.verify(navController).navigate(
            ShoppingFragmentDirections.actionShoppingFragmentToAddShoppingItemFragment()
        )
    }
}

