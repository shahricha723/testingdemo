package com.androiddevs.demo.ui.shopping

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.Espresso.pressBack
import androidx.test.espresso.action.ViewActions
import androidx.test.espresso.matcher.ViewMatchers.isRoot
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.MediumTest
import com.androiddevs.demo.R.id
import com.androiddevs.demo.data.local.ShoppingItem
import com.androiddevs.demo.getOrAwaitValue
import com.androiddevs.demo.helper.launchFragmentInHiltContainer
import com.androiddevs.demo.helper.waitFor
import com.androiddevs.demo.repositories.FakeShoppingRepositoryAndroidTest
import com.google.common.truth.Truth.assertThat
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.*
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.verify
import javax.inject.Inject


@MediumTest
@HiltAndroidTest
@ExperimentalCoroutinesApi
class AddShoppingItemFragmentTestTwo
{
    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @Inject
    lateinit var fragmentFactory: TestShoppingFragmentFactory

    @Before
    fun setup()
    {
        hiltRule.inject()
    }

    @Test
    fun clickInsertIntoDb_shoppingItemInsertedIntoDb()
    {
        val testViewModel = ShoppingViewModel(FakeShoppingRepositoryAndroidTest())
        launchFragmentInHiltContainer<AddShoppingItemFragment>(
            fragmentFactory = fragmentFactory
        ) {
            viewModel = testViewModel
        }

        Espresso.onView(withId(id.etShoppingItemName))
            .perform(ViewActions.replaceText("shopping item"))
        Espresso.onView(withId(id.etShoppingItemAmount)).perform(ViewActions.replaceText("5"))
        Espresso.onView(withId(id.etShoppingItemPrice)).perform(ViewActions.replaceText("5.5"))
        onView(isRoot()).perform(waitFor(3000))

        Espresso.onView(withId(id.btnAddShoppingItem)).perform(ViewActions.click())

        assertThat(testViewModel.shoppingItems.getOrAwaitValue())
            .contains(ShoppingItem("shopping item", 5, 5.5f, ""))
    }
    @Test
    fun searchApi_Testing()
    {
        val testViewModel = ShoppingViewModel(FakeShoppingRepositoryAndroidTest())
        launchFragmentInHiltContainer<AddShoppingItemFragment>(
            fragmentFactory = fragmentFactory
        ) {
            viewModel = testViewModel
            CoroutineScope(Dispatchers.IO).launch {
                delay(500L)
            }
        }
        onView(isRoot()).perform(waitFor(3000))



//        assertThat(testViewModel.images.getOrAwaitValue()).isNotNull()
    }
    @Test
    fun pressBackButton_popBackStack()
    {
        val navController = Mockito.mock(NavController::class.java)
        launchFragmentInHiltContainer<AddShoppingItemFragment>(
            fragmentFactory = fragmentFactory
        ) {
            Navigation.setViewNavController(requireView(), navController)
        }

        pressBack()

        verify(navController).popBackStack()
    }
}