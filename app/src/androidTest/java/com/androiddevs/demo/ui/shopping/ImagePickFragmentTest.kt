package com.androiddevs.demo.ui.shopping

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.navigation.NavController
import androidx.navigation.Navigation
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.filters.MediumTest
import com.androiddevs.demo.R
import com.androiddevs.demo.adapters.ImageAdapter
import com.androiddevs.demo.getOrAwaitValue
import com.androiddevs.demo.helper.launchFragmentInHiltContainer
import com.androiddevs.demo.helper.waitFor
import com.androiddevs.demo.repositories.FakeShoppingRepositoryAndroidTest
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.*
import org.junit.*
import org.mockito.Mockito

@MediumTest
@HiltAndroidTest
@ExperimentalCoroutinesApi
class ImagePickFragmentTest
{

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Before
    fun setUp()
    {
        hiltRule.inject()

    }


    @Test
    fun clickImage_popBackStackedSetImageUrl()
    {
        val navController = Mockito.mock(NavController::class.java)
        val testUrl = "https://picsum.photos/200"
        val testViewModel = ShoppingViewModel(FakeShoppingRepositoryAndroidTest())
        launchFragmentInHiltContainer<ImagePickFragment> {
            Navigation.setViewNavController(requireView(), navController)
            imageAdapter.images = arrayListOf(testUrl, testUrl)
            viewModel = testViewModel

        }

        onView(withId(R.id.rvImages)).perform(
            RecyclerViewActions
                .actionOnItemAtPosition<ImageAdapter
                .ImageViewHolder>(
                    0,
                    click()
                )
        )
        onView(ViewMatchers.isRoot()).perform(waitFor(2000))

        Mockito.verify(navController).popBackStack()


    }

    @Test
    fun searchApi_Testing()
    {
        val navController = Mockito.mock(NavController::class.java)
//        val testUrl = "https://picsum.photos/200"
        val testViewModel = ShoppingViewModel(FakeShoppingRepositoryAndroidTest())
        launchFragmentInHiltContainer<ImagePickFragment> {
            Navigation.setViewNavController(requireView(), navController)
//            imageAdapter.images = arrayListOf(testUrl, testUrl)
            viewModel = testViewModel
            CoroutineScope(Dispatchers.IO).launch {
                viewModel.searchForImage("a")
                delay(2000)
            }

        }

        onView(ViewMatchers.isRoot()).perform(waitFor(2000))

        Assert.assertTrue(
            testViewModel.images.getOrAwaitValue().getContentIfNotHandled()?.data != null
        )

    }

    @After
    fun tearDown()
    {
    }


}