package com.androiddevs.demo.other

object Constants {

    const val DATABASE_NAME = "shopping_db"

    const val BASE_URL = "https://pixabay.com"
    const val MOCK_BASE_URL = "https://demo1732035.mockable.io/"

    const val MAX_NAME_LENGTH = 20
    const val MAX_PRICE_LENGTH = 10

    const val GRID_SPAN_COUNT = 4
}