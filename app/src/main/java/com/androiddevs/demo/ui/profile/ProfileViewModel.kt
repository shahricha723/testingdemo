package com.androiddevs.demo.ui.profile

import androidx.lifecycle.ViewModel
import com.androiddevs.demo.data.model.Profile
import com.androiddevs.demo.repositories.DataRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ProfileViewModel @Inject constructor(
    private val repo: DataRepository
) : ViewModel() {

    fun getProfile(): Profile = repo.getProfile()

}