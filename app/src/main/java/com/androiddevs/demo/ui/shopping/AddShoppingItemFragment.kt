package com.androiddevs.demo.ui.shopping

import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.androiddevs.demo.R
import com.google.android.material.button.MaterialButton
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_add_shopping_item.*

@AndroidEntryPoint
class AddShoppingItemFragment : Fragment(R.layout.fragment_add_shopping_item) {

    lateinit var viewModel: ShoppingViewModel


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel = ViewModelProvider(requireActivity()).get(ShoppingViewModel::class.java)

        view.findViewById<ImageView>(R.id.ivShoppingImage).setOnClickListener {
            findNavController().navigate(
                R.id.action_addShoppingItemFragment_to_imagePickFragment
            )
        }
  view.findViewById<MaterialButton>(R.id.btnAddShoppingItem).setOnClickListener {
      viewModel.insertShoppingItem(
          etShoppingItemName.text.toString(),
          etShoppingItemAmount.text.toString(),
          etShoppingItemPrice.text.toString()
      )
        }

        val callback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                viewModel.setCurImageUrl("")
                findNavController().popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(callback)
    }
}