package com.androiddevs.demo.repositories

import androidx.lifecycle.LiveData
import com.androiddevs.demo.data.local.ShoppingItem
import com.androiddevs.demo.data.remote.responses.ImageResponse
import com.androiddevs.demo.other.Resource

interface ShoppingRepository {

    suspend fun insertShoppingItem(shoppingItem: ShoppingItem)

    suspend fun deleteShoppingItem(shoppingItem: ShoppingItem)

    fun observeAllShoppingItems(): LiveData<List<ShoppingItem>>

    fun observeTotalPrice(): LiveData<Float>

    suspend fun searchForImage(imageQuery: String): Resource<ImageResponse>
}