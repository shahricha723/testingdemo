package com.androiddevs.demo.repositories

import com.androiddevs.demo.data.model.Profile
import javax.inject.Inject

class DataRepoImpl @Inject constructor() : DataRepository
{

    override fun getProfile(): Profile =
        Profile(name = "Bruce Wayne", age = 42)
}