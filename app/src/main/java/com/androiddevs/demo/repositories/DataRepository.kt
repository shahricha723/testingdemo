package com.androiddevs.demo.repositories

import com.androiddevs.demo.data.model.Profile

interface DataRepository {

    fun getProfile(): Profile
}