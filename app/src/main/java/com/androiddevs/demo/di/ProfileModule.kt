package com.androiddevs.demo.di

import com.androiddevs.demo.repositories.DataRepoImpl
import com.androiddevs.demo.repositories.DataRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent

@InstallIn(ViewModelComponent::class)
@Module
abstract class ProfileModule
{
    @Binds
    abstract fun getProfileSource(repo: DataRepoImpl): DataRepository
}