package com.androiddevs.demo.data.remote.responses


import com.google.gson.annotations.SerializedName

class LocationResp : ArrayList<LocationRespItem>()
data class LocationRespItem(
    @SerializedName("icon")
    var icon: String? = null,
    @SerializedName("latLng")
    var latLng: LatLng? = null,
    @SerializedName("primaryText")
    var primaryText: String? = null,
    @SerializedName("secondaryText")
    var secondaryText: String? = null
)

data class LatLng(
    @SerializedName("latitude")
    var latitude: String? = null,
    @SerializedName("longitude")
    var longitude: String? = null
)
