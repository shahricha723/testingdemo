package com.androiddevs.demo.data.mock

import com.androiddevs.demo.data.remote.PixabayAPI

class JsonRepository(private val api: PixabayAPI)
{
    suspend fun observeImages() = api.searchForImage("a")


}