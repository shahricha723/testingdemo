package com.androiddevs.demo.data.remote

import com.androiddevs.demo.data.remote.responses.ImageResponse
import com.androiddevs.demo.data.remote.responses.LocationResp
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface PixabayAPI {

    @GET("/api/")
     fun searchForImage(
        @Query("q") searchQuery: String,
        @Query("key") apiKey: String = "28066941-b071228f474ce1f9a04f5df53"
    ): Response<ImageResponse>

    @GET("/location")
    fun getCitiesList(): Response<LocationResp>
}