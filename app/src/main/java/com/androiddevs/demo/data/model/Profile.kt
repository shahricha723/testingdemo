package com.androiddevs.demo.data.model

data class Profile(
    val name: String,
    val age: Int
)
